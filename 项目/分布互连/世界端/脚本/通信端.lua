--===================================================
--通信服务端，给登陆网关，或者 转发网关连接的
--===================================================
local mp = require("MessagePack")
__通信服务 = require("PackServer")('通信服务')

__通信服务.发送_ = __通信服务.发送

function __通信服务:发送(id,...)
    self:发送_(id,mp.pack{...})
end
function __通信服务:发送登录(...)
    self:发送_(self.登陆id,mp.pack{...})
end

function __通信服务:启动成功()

end

function __通信服务:连接进入(id,ip,port)

end

function __通信服务:连接退出(id,so,ec)

end

function __通信服务:数据到达(id,data)
    data = mp.unpack(data)
    local i,d = unpack(data)
    if i==0 then
        if d == '登陆端连接' then
            self.登陆id = id
        end
    end
    print("数据到达",id,unpack(data))
end

__通信服务:启动('127.0.0.1',9528)--内网

--====================================================
--用于连接数据端，或者聊天端
--====================================================
__通信客户 = require("PackAgent")('通信客户')


function __通信客户:连接成功(id)
    local ip ,pr = self:取远程地址信息(id)
    if pr == 9527 then
        self.数据id = id
    elseif pr == 9529 then
        self.聊天id = id
    end
end
function __通信客户:数据到达(id,...)

end
function __通信客户:发送数据(...)

    self:发送(self.数据id,mp.pack{...})
end
function __通信客户:发送聊天(...)
    self:发送(self.聊天id,mp.pack{...})
end
__通信客户:启动()
__通信客户:连接('127.0.0.1',9527,'数据')--连接数据
--__通信客户:连接('127.0.0.1',9529,'聊天')--连接聊天

__通信客户:发送数据(0,'世界端连接')
