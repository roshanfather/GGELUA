-- @Author: baidwwy
-- @Date:   2018-04-18 09:33:54
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-29 08:01:56
sublime_project = [[
{
    "folders":
    [
        {
            "file_exclude_patterns":
            [
                "项目名.sublime-project"
            ],
            "folder_exclude_patterns":
            [
                "compile"
            ],
            "path": "."
        },
        {
            "path": "../../脚本"
        }
    ]
}
]]
sublime_workspace = [[
{
    "buffers":
    [
        {
            "file": "gge.lua",
        },
        {
            "file": "main.lua",
        }
    ],
    "build_system": "Packages/Lua/编译系统.sublime-build",
    "pinned_build_system": "Packages/Lua/编译系统.sublime-build",
    "project": "项目名.sublime-project",
    "groups":
    [
        {
            "selected": 1,
            "sheets":
            [
                {
                    "buffer": 0,
                    "file": "gge.lua",
                    "semi_transient": false,
                    "stack_index": 1,
                    "type": "text"
                },
                {
                    "buffer": 1,
                    "file": "main.lua",
                    "semi_transient": false,
                    "stack_index": 0,
                    "type": "text"
                }
            ]
        }
    ],
}
]]
game_main = [[
require("GGE")--引用头

引擎{
    标题 = "游戏模版",
    宽度 = 800,高度=600,
    帧率 = 60
}


function 引擎:更新函数(dt,x,y)--帧率,鼠标x,鼠标y

end

function 引擎:渲染函数(x,y)--鼠标x,鼠标y
    if 引擎.渲染开始() then
        引擎.渲染清除(0xFF272822)

        引擎.渲染结束()
    end
end

function 引擎:按键函数(key,name)

end

function 引擎:退出函数()
    return true
end

]]
bc_main = [[

引擎{
    标题 = "游戏模版",
    宽度 = 800,高度=600,
    帧率 = 60
}


function 引擎:更新函数(dt,x,y)--帧率,鼠标x,鼠标y

end

function 引擎:渲染函数(x,y)--鼠标x,鼠标y
    if 引擎.渲染开始() then
        引擎.渲染清除(0xFF272822)

        引擎.渲染结束()
    end
end
]]
game_gge = [[
require("gge函数")



function 引擎:创建完成()

end
]]
server_main = [[
require("GGE")--引用头

引擎{标题 = '世界服务端'}




function 引擎:循环函数()

end

function 引擎:输入函数(t)

end
]]
server_gge = [[
local mp = require("MessagePack")
__服务 = require("PackServer")('测试')

-- __服务.发送_ = __服务.发送

-- function __服务:发送(...)
--     self:发送_(mp.pack{...})
-- end
function __服务:启动成功()

end

function __服务:连接进入(id,ip,port)
    print('连接进入',id,ip,port)
    self:发送(id,"欢迎进入。")
end

function __服务:连接退出(id,so,ec)

end

function __服务:数据到达(id,data)

    print("数据到达",id,data)
end

__服务:启动('127.0.0.1',9876)
]]
os.execute'cls'
ffi.cdef[[
    void SHChangeNotify(int,int,int,int);
]]
shell32 = ffi.load('shell32.dll')
require("winapi")
k,err = winapi.open_reg_key ([[HKEY_CLASSES_ROOT\sublime-project\DefaultIcon\]],true)
if k then
    if k:get_value()~=__gge.runpath..[[\编辑器\SG.ico]] then
        k:set_value('',__gge.runpath..[[\编辑器\SG.ico]],winapi.REG_SZ)
        k:close()
        k,err = winapi.open_reg_key ([[HKEY_CLASSES_ROOT\sublime-project\shell\open\command\]],true)
        k:set_value('',__gge.runpath..[[\编辑器\sublime_text.exe %1]],winapi.REG_SZ)
        k:close()
        shell32.SHChangeNotify(134217728,0,0,0)--SHCNE_ASSOCCHANGED
    end
else
    print('关联项目失败',err)
end
ffi.C.SetConsoleTitleA('GGELUA 项目创建器')


    local logo = [[
  /$$$$$$   /$$$$$$  /$$$$$$$$       /$$       /$$   /$$  /$$$$$$
 /$$__  $$ /$$__  $$| $$_____/      | $$      | $$  | $$ /$$__  $$
| $$  \__/| $$  \__/| $$            | $$      | $$  | $$| $$  \ $$
| $$ /$$$$| $$ /$$$$| $$$$$         | $$      | $$  | $$| $$$$$$$$
| $$|_  $$| $$|_  $$| $$__/         | $$      | $$  | $$| $$__  $$
| $$  \ $$| $$  \ $$| $$            | $$      | $$  | $$| $$  | $$
|  $$$$$$/|  $$$$$$/| $$$$$$$$      | $$$$$$$$|  $$$$$$/| $$  | $$
 \______/  \______/ |________/      |________/ \______/ |__/  |__/ Creator
]]
print(logo)

print[[请输入要创建的项目:
  1.创建游戏端
  2.创建服务端
  3.创建支持库
  0.退出
]]

require("gge函数")
while true do
    s = io.read()
    if s == '1' then
        print('请输入项目名称:')
        s = io.read()
        if #s>0 then
            项目目录 = "./项目/"..s..'/'
            os.mkdir(项目目录.."脚本/")
            os.mkdir(项目目录.."资源/")
            os.mkdir(项目目录)
            __gge.writefile(项目目录..s..".sublime-project",
                __gge.ansitoutf8((sublime_project:gsub('项目名',s)))
            )
            __gge.writefile(项目目录..s..".sublime-workspace",
                __gge.ansitoutf8((sublime_workspace:gsub('项目名',s):gsub("编译系统",'ggegame')))
            )
            __gge.writefile(项目目录.."main.lua",
                __gge.ansitoutf8(game_main)
            )
            __gge.writefile(项目目录.."gge.lua",
                __gge.ansitoutf8(game_gge)
            )
            os.execute('start '..项目目录..s..".sublime-project")
        end
        break
    elseif s == '2' then
        print('请输入项目名称:')
        s = io.read()
        if #s>0 then
            项目目录 = "./项目/"..s..'/'
            os.mkdir(项目目录.."脚本/")
            os.mkdir(项目目录.."资源/")
            os.mkdir(项目目录.."log/")
            os.mkdir(项目目录)
            __gge.writefile(项目目录..s..".sublime-project",
                __gge.ansitoutf8((sublime_project:gsub('项目名',s)))
            )
            __gge.writefile(项目目录..s..".sublime-workspace",
                __gge.ansitoutf8((sublime_workspace:gsub('项目名',s):gsub("编译系统",'ggeserver')))
            )
            __gge.writefile(项目目录.."main.lua",
                __gge.ansitoutf8(server_main)
            )
            __gge.writefile(项目目录.."gge.lua",
                __gge.ansitoutf8(server_gge)
            )
            os.execute('start '..项目目录..s..".sublime-project")
        end
        break
    elseif s == '3' then
        print('请输入项目名称:')
        s = io.read()
        if #s>0 then
            项目目录 = "./项目/"..s..'/'
            os.mkdir(项目目录.."脚本/")
            os.mkdir(项目目录)
            __gge.writefile(项目目录..s..".sublime-project",
                __gge.ansitoutf8((sublime_project:gsub('项目名',s)))
            )
            __gge.writefile(项目目录..s..".sublime-workspace",
                __gge.ansitoutf8((sublime_workspace:gsub('项目名',s):gsub("编译系统",'ggebc')))
            )
            __gge.writefile(项目目录.."main.lua",
                __gge.ansitoutf8(bc_main)
            )
            os.execute('start '..项目目录..s..".sublime-project")
        end
        break
    elseif s == '0' then
        break
    else
        print('输入错误,重新输入。')
    end
end







