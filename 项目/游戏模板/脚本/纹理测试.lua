-- @Author: baidwwy
-- @Date:   2018-04-11 16:59:57
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-17 04:04:11

纹理  = require("gge纹理类")("资源/test.jpg")
print(纹理,纹理:是否有效())

print(纹理:锁定(),纹理:是否已锁定());
print(纹理:解锁(),纹理:是否已锁定())
print(纹理:取宽度(),纹理:取高度())

纹理:复制()
    :保存到文件('./复制.jpg','JPG')
纹理:复制区域(0,0,400,300)
    :保存到文件('./复制区域.jpg','JPG')
纹理:灰度级()
    :保存到文件('./灰度级.jpg','JPG')

纹理:置颜色(0xFF8D8D8D)
    :保存到文件('./置颜色.jpg','JPG')
