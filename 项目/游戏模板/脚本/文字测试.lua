-- @Author: baidwwy
-- @Date:   2018-04-12 16:40:10
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-12 16:48:32
文字 = require("gge文字类")(os.getenv("SystemRoot")..'/Fonts/simsun.ttc',14)
粗体文字 = require("gge文字类")(os.getenv("SystemRoot")..'/Fonts/simsun.ttc',14,true)
描边文字 = require("gge文字类")(os.getenv("SystemRoot")..'/Fonts/simsun.ttc',16,false,true)
    :置描边颜色(0xFF000000)
阴影文字 = require("gge文字类")(os.getenv("SystemRoot")..'/Fonts/simsun.ttc',14)
    :置阴影颜色(0xFF000000)
