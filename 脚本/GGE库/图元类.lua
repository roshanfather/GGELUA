-- @Author: baidwwy
-- @Date:   2018-04-27 10:47:05
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-27 23:09:59
ffi.cdef[[
    /// 顶点定义
    typedef struct
    {
        float   x, y;   ///< 屏幕坐标
        float   z;      ///< Z缓冲(0~1)
        uint32_t c;     ///< 顶点颜色
        float   tx, ty; ///< 纹理坐标
    }gge顶点;
]]
local _ggeVertex = ffi.typeof('gge顶点[?]')
local _顶点类 = class()
    function _顶点类:初始化(_x,_y,_c,_tx,_ty)
        self.x = _x or 0
        self.y = _y or 0
        self.c = _c
        self.tx = _tx
        self.ty = _ty
    end
    function _顶点类:复制()
        return _顶点类(self.x,self.y,self.c,self.tx,self.ty)
    end
    function _顶点类:取距离坐标(r,a) --距离,孤度
        local x,y
        x=r* math.cos(a)+self.x
        y=r* math.sin(a)+self.y
        return _顶点类(x,y)
    end
--==============================================================
local 图元类 = class()
rawset(图元类,'顶点类',_顶点类)
local function 创建(self,v)
    for i,v in ipairs(v) do
        --print(i,v.x,v.y)
        if not v.c then
            v.c = 0xFFFFFFFF
        end
    end
    self.vd = v
    self.vt = _ggeVertex(#v,v)
    self.vtn = #v-1 --不知道为什么。。。
end
function 图元类:创建点(v)
    self.type = 1
    创建(self,v)
    return self
end
function 图元类:创建线(v)
    self.type = 2
    创建(self,v)
    return self
end
function 图元类:创建三角形(v)
    self.type = 3
    创建(self,v)
    return self
end
function 图元类:创建四边形(v)
    self.type = 4
    创建(self,v)
    return self
end
function 图元类:创建线带(v)
    self.type = 5
    创建(self,v)
    return self
end
function 图元类:创建三角带(v)
    self.type = 6
    创建(self,v)
    return self
end
function 图元类:创建扇形(v)
    self.type = 7
    创建(self,v)
    return self
end
function 图元类:置纹理(v)
    self.tex = v
    return self
end
function 图元类:置混合(v)
    self.blend = v
    return self
end
function 图元类:置颜色(v,i)
    if i then
        self.vt[i].c = v
    else
        for i=0,#self.vd-1 do
            self.vt[i].c = v
        end
    end
    return self
end
function 图元类:显示(x,y)
    if x~=self.x or y~=self.y then
        self.x = x;self.y=y
        for i,v in ipairs(self.vd) do
            self.vt[i-1].x = v.x +x
            self.vt[i-1].y = v.y +y
        end
    end
    引擎.画图元(self.type,self.vt,self.vtn,self.tex,self.blend)
end

return 图元类