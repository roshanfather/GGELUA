-- @Author: baidwwy
-- @Date:   2017-08-22 19:07:39
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-18 09:27:17
__颜色 = {
    深蓝 = 1,
    深绿 = 2,
    深青 = 3,
    深红 = 4,
    深紫 = 5,
    深黄 = 6,
    深白 = 7,
    深灰 = 8,

    蓝色 = 9,
    绿色 = 10,
    青色 = 11,
    红色 = 12,
    紫色 = 13,
    黄色 = 14,
    白色 = 15
}
local _input,_exit = {},false
function __Loop(t, ... )
    if t then
        if t == 'close' then
            if 引擎.退出函数 then
                __gge.xpcall(引擎.退出函数,引擎)
            end
        elseif 引擎.输入函数 and type(t)=='number' then
            if t==13 then
                if _input[1] and __gge.xpcall(引擎.输入函数,引擎,string.char(unpack(_input))) then
                    return true
                end
                _input = {}
            else
                table.insert(_input, t)
            end
        end
        return false
    elseif 引擎.循环函数 then
        return __gge.xpcall(引擎.循环函数,引擎) or _exit
    end
    return _exit
end
--================================================================
引擎 = {}

function 引擎.创建(t)
    if t.标题 then 引擎.置标题(t.标题) end
    if t.日志 then 引擎.日志 = require("logger")(t.日志) end
    __gge.start(__Loop)
end
function 引擎.输出(文本,颜色)
    __gge.print(true,颜色 or __颜色.深白,文本.."\n")
end
function 引擎.日志(文本,类型)
    if 引擎.日志 then
        引擎.日志:log(文本,类型)
    end
end
function 引擎.置标题(str)
    ffi.C.SetConsoleTitleA(str)
end
function 引擎.置延迟()

end
function 引擎.文件是否存在(str)
    return __gge.fileexist(str)
end

function 引擎.关闭()
    _exit = true
end
setmetatable(引擎,{__call = function (t, ... )
    return t.创建(...)
end
})

return 引擎