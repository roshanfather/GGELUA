-- @Author: baidwwy
-- @Date:   2018-04-10 22:52:12
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-18 08:16:16

local __ggethread = require("gge_thread")

local thread = class()

function thread:初始化(脚本)
    self.obj = __ggethread(脚本,self)
end

function thread:启动(队列数)
    return self.obj:Start(队列数 or 1000)
end

function thread:停止()
    self.obj:Stop()
end

function thread:置延迟(v)
    self.obj:SetSleep(v)
end
-- function thread:同步发送(...)
--     if self.是否启动 then
--         self.obj:SyncSend(...)
--     end
-- end
function thread:发送(data)
    return self.obj:Send(data)
end
function thread:OnMessage(...)
    if self.消息函数 then
        return __gge.xpcall(self.消息函数,self,...)
    end
end
return thread