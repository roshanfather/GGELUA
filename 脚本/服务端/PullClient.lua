-- @Author: baidwwy
-- @Date:   2018-04-10 22:52:12
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-17 21:05:38

package.loaded['luahp.client'] = require("gge_hpclient")
local base = require("hp/PullClient")
local GGELUA = class(base)

function GGELUA:初始化(t)
    self._title     = t or ''
    __gge.print(false,7,"----------------------------------------------------------------------------\n")
    __gge.print(true,7,string.format("%-58s[", string.format("创建%s客户端", self._title)))
    __gge.print(false,10,"成功")
    __gge.print(false,7,']\n')
end

function GGELUA:连接(ip,port,async)
    __gge.print(false,__颜色.深白,"----------------------------------------------------------------------------\n")
    __gge.print(true,__颜色.深白,string.format("%-58s[", string.format('连接%s"%s:%s"', self._title,ip,port)))
    if not self[base]:连接(ip,port,async) then
        __gge.print(false,__颜色.红色,"失败")
        __gge.print(false,__颜色.深白,"]\7\n")
        return false
    end
    return true
end

function GGELUA:OnConnect()
    __gge.print(false,__颜色.绿色,"成功")
    __gge.print(false,__颜色.深白,"]\n")
    self[base]:OnConnect()
end

function GGELUA:OnClose(so,ec)
    if so == 2 then--异步连接
        __gge.print(false,12,"失败！\n")
    end
    self[base]:OnClose()
end
return GGELUA