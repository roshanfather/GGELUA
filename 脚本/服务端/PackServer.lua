-- @Author: baidwwy
-- @Date:   2018-04-17 10:20:22
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-17 21:15:28
package.loaded['luahp.server'] = require("gge_hpserver")
local base = require("hp/PackServer")
local GGELUA = class(base)

function GGELUA:初始化(t)
    self._title   = t or ''
    __gge.print(false,__颜色.深白,"----------------------------------------------------------------------------\n")
    __gge.print(true,__颜色.深白,string.format("%-58s[", string.format("PackServer->创建%s", self._title)))
    __gge.print(false,__颜色.绿色,"成功")
    __gge.print(false,__颜色.深白,"]\n")
end

function GGELUA:启动(ip,port)
    __gge.print(false,__颜色.深白,"----------------------------------------------------------------------------\n")
    __gge.print(true,__颜色.深白,string.format("%-58s[", string.format('PackServer->启动%s"%s:%s"', self._title,ip,port)))
    if not self[base]:启动(ip,port) then
        __gge.print(false,__颜色.红色,"失败")
        __gge.print(false,__颜色.深白,"]\7\n")
        return false
    end
    return true
end

function GGELUA:OnPrepareListen(soListen)--启动成功
    __gge.print(false,10,"成功");__gge.print(false,__颜色.深白,"]\n")
    __gge.print(false,__颜色.深白,"----------------------------------------------------------------------------\n")
    __gge.print(false,__颜色.深白,"工作线程数量:")
    __gge.print(false,11,self:取工作线程数量())
    __gge.print(false,__颜色.深白,"|并发请求数量:")
    __gge.print(false,11,self:取预投递数量())
    __gge.print(false,__颜色.深白,"|缓冲区大小:")
    __gge.print(false,11,self:取缓冲区大小())
    __gge.print(false,__颜色.深白,"|等候队列大小:")
    __gge.print(false,11,self:取等候队列大小().."\n")
    __gge.print(false,__颜色.深白,"----------------------------------------------------------------------------\n")

    return self[base]:OnPrepareListen(soListen)
end

return GGELUA