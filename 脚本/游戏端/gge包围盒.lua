-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-30 17:36:06
--gge包围盒
--======================================================================--
--local _tostring = function (t) return "ggerect" end
--local _eq       = function (a,b) return 123 end
--======================================================================--
local GGELUA    = class()
GGELUA._new     = require("gge_rect")


--x,y,宽度,高度
function GGELUA:初始化(x,y,w,h)
    self._obj = self._new()
    if x and y and w and h then
        self._obj:SetWH(x,y,w,h)
    elseif type(x)=='userdata' and tostring(x)=='ggeRect' then
        self._obj:SetRect(x)
    else
        self._obj:Clear()
    end
    -- local mt = getmetatable(self)
    --     mt.__tostring = _tostring
    --     mt.__eq = _eq
end
-- @brief 设置矩形坐标
-- @param _x1 左上角x坐标
-- @param _y1 左上角y坐标
-- @param _x2 右下角x坐标
-- @param _y2 右下角y坐标
function GGELUA:置两点坐标(x,y,x1,y1)
    self._obj:SetXY(x,y,x1,y1)
    return self
end
function GGELUA:取两点坐标()
    return self._obj:GetXY()
end
function GGELUA:置坐标宽高(x,y,w,h)
    self._obj:SetWH(x,y,w,h)
    return self
end
function GGELUA:取坐标宽高()
    return self._obj:GetWH()
end

--@brief 清除矩形
-- function GGELUA:重置()
--     self._obj:Clear()
--     return self
-- end
-- @brief 矩形是否有效
-- @return 矩形是否有效
function GGELUA:是否有效()
    return self._obj:IsClean()
end
-- @brief 移动矩形到点(x, y)，矩形大小不变
-- @param x x坐标
-- @param y y坐标
function GGELUA:置坐标(x,y)
    if type(x) == 'table' then
        x,y = x.x,x.y
    end
    self._obj:Move(x,y)
    return self
end
function GGELUA:取坐标()
    local x,y = self._obj:GetXY()
    return x,y
end
function GGELUA:置宽度(w)
    self._obj:SetWidth(w)
    return self
end
function GGELUA:取宽度()
    return self._obj:GetWidth()
end
function GGELUA:置高度(h)
    self._obj:SetHeight(h)
    return self
end
function GGELUA:取高度()
    return self._obj:GetHeight()
end
-- @brief 设置矩形范围
-- @param x 矩形中心x坐标
-- @param y 矩形中心y坐标
-- @param r 矩形范围
function GGELUA:置半径(x,y,r)
    self._obj:SetRadius(x,y,r)
    return self
end
-- @brief 调整矩形大小使其能够包含点(x, y)
-- @param x x坐标
-- @param y y坐标
function GGELUA:包含点(x,y)
    self._obj:Encapsulate(x,y)
    return self
end
-- @brief 测试点(x, y)是否在该矩形框内
-- @param x x坐标
-- @param y y坐标
-- @return 点是否在该矩形框内
function GGELUA:检查点(x,y)
    return self._obj:TestPoint(x,y)
end
-- @brief 测试两个矩形框是否相交
-- @param rect 要测试的矩形
-- @return 两个矩形框是否相交
function GGELUA:检查盒(rect)
    return self._obj:Intersect(rect:取对象())
end
--======================================================================================
--扩展
--======================================================================================
function GGELUA:置中心(x,y)
    self._obj:SetHotSpot(x,y)
    return self
end
function GGELUA:取中心()
    return self._obj:GetHotSpot()
end
function GGELUA:置宽高(x,y)
    self._obj:SetSize(x,y)
    return self
end
function GGELUA:取宽高()
    local _,_,w,h self._obj:GetWH()
    return w,h
end
function GGELUA:显示(c)
    local x,y,x1,y1 = self:取两点坐标()
    引擎.画线(x,y,x1,y,c)--上
    引擎.画线(x,y1,x1,y1,c)--下
    引擎.画线(x,y,x,y1,c)--左
    引擎.画线(x1,y,x1,y1,c)--右
end

-------------------------------------------------------------------
function GGELUA:置指针(p)
    self._obj:SetP(p)
    return self
end
function GGELUA:取指针()
    return self._obj:GetP()
end
function GGELUA:取对象()
    return self._obj
end
return GGELUA