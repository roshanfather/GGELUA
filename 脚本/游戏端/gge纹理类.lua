-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-30 12:35:01

--<<=========================================================================================>>--
local _tostring     = function (t) return "texture",tostring(t._obj) end
--<<=========================================================================================>>--

local _纹理列表 = 引擎 and 引擎.纹理列表

local GGELUA = class(require"gge引用")
GGELUA._new_ = require("gge_texture")
--[[
参数1 gge纹理指针
参数2 文件路径,透明色
参数3 内存指针,长度,透明色
参数3 string,长度,透明色
]]
function GGELUA:初始化(c1,c2,c3)
    self._obj = self._new_()
    if c1 then
        local lt = type(c1)
        if(lt == "string")then
            if #c1==c2 or #c1>255 then--2为指针长度--按luastring指针
                assert(self._obj:Texture_LoadStr(c1,c2 or 0), "载入失败.")
            else--2为透明颜色--按文件路径载入
                assert(self._obj:Texture_LoadFile(c1,c2 or 0), '载入失败:'.. c1)
            end
        elseif(lt == "number")then  --按指针载入
            if type(c2)  == "number" then--2为指针长度
                assert(self._obj:Texture_LoadMem(c1,c2,c3 or 0), "载入失败.")--按图片指针
            else
                self._obj:SetP(c1)--按gge纹理指针
            end
        elseif(lt == "cdata")then--ffi
            assert(self._obj:Texture_LoadMem(ffi.getptr(c1),c2,c3 or 0), "载入失败.")
        elseif(lt == "userdata")then
            if tostring(c1)=='ggeTexture' then
                self._obj = c1
            else
                self._obj:Texture_Pointer(c1)
            end
        end
    end
    local mt = getmetatable(self)
    mt.__tostring   = _tostring
    if _纹理列表 then _纹理列表[self] = self end
end
-- @brief 锁定纹理
-- @param bReadOnly 是否只读，若为true该纹理在解锁之后不会更新(如果不需要写入数据，可将该值设为true以提高效率)
-- @param left 设置锁定区域左边位置
-- @param top 设置锁定区域上方位置
-- @param width 设置锁定区域宽度
-- @param height 设置锁定区域高度
-- @return 成功返回纹理数据(格式：32位/ARGB)，否则返回0
function GGELUA:锁定(bReadOnly,left,top,width,height)
    return self._obj:Lock(bReadOnly==nil and true or bReadOnly,left or 0,top or 0,width or 0,height or 0)
end
-- @brief 解除纹理锁定
function GGELUA:解锁()
    self._obj:Unlock()
    return self
end
function GGELUA:是否已锁定()
    return self._obj:IsLock()
end
-- @brief 获得纹理宽度
-- @param bOrginal 为true时返回纹理原始宽度，为false时返回纹理在显存宽度
-- @return 纹理宽度
function GGELUA:取宽度(bOrginal)
    return self._obj:GetWidth(bOrginal)
end
-- @brief 获得纹理高度
-- @param bOrginal 为true时返回纹理原始高度，为false时返回纹理在显存高度
-- @return 纹理高度
function GGELUA:取高度(bOrginal)
    return self._obj:GetHeight(bOrginal)
end
-- @brief 用指定的颜色填充纹理
-- @param color 填充色
function GGELUA:置颜色(color)
    self._obj:FillColor(color)
    return self
end
-- @brief 保存到文件
-- @param filename 文件名
-- @param imageFormat 文件格式，默认保存为PNG格式图片
-- @return 返回保存是否成功
local GGE_IMAGE_FORMAT =
{
    JPG  = 0, --///< jpg文件
    PNG  = 1, --///< png文件
    BMP  = 2, --///< bmp文件
    TGA  = 3, --///< tga文件
    DXT1 = 4,--///< dds文件
    DXT3 = 5,--///< dds文件
    DXT5 = 6,--///< dds文件
};
function GGELUA:保存到文件(filename,imageFormat)
    return self._obj:SaveToFile(filename,GGE_IMAGE_FORMAT[imageFormat] or 3)
end
-- @brief 创建纹理
-- @param width 纹理宽度
-- @param height 纹理高度
-- @return 成功返回ggeTexture指针，失败返回0
function GGELUA:空白纹理(width,height,c)--宽度,高度,颜色
    self._obj:Texture_Create(width or 0,height or 0)
    if(c)then self._obj:FillColor(c) end
    return self
end
-- /// 渲染目标纹理类型
-- enum TARGET_TYPE
-- {
--  TARGET_DEFAULT  = 0,    ///< 默认类型
--  TARGET_ZBUFFER  = 1,    ///< 开启ZBuffer
--  TARGET_LOCKABLE = 2,    ///< 渲染目标纹理可以被锁定
--  TARGET_ALPHA    = 4,    ///< 渲染目标纹理带Alpha通道(可通过系统状态 GGE_ALPHARENDERTARGET 检测显卡是否支持创建带Alpha通道的渲染目标纹理)
--  TARGET_FORCE32BIT = 0x7FFFFFFF,
-- };
-- 可以用加法结合，比如 4+2
function GGELUA:渲染目标(width,height,targetType)--宽度,高度,ZBuffer,是否是可以锁定,Alpha
    assert(self._obj:Texture_CreateRenderTarget(width,height,targetType or 6),"创建失败")
    return self
end
--====================================================================================
--扩展
--====================================================================================
--顾名思义
function GGELUA:复制()
    local p = self._obj:Copy()
    if p then
        return GGELUA(p)
    end
end
--顾名思义
function GGELUA:复制区域(x,y,width,height)
    local p = self._obj:CopyRect(x,y,width,height)
    if p then
        return GGELUA(p)
    end
end
--转化成黑白图片
function GGELUA:灰度级(new)
    if new then
        return self:复制():灰度级()
    end
    self._obj:Grayscale()
    return self
end
--顾名思义
function GGELUA:置像素(x,y,c)--宽度,高度,颜色
    self._obj:SetPixel(x,y,c)
end
--顾名思义
function GGELUA:取像素(x,y)
    return self._obj:GetPixel(x,y)
end
return GGELUA