-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-16 21:13:39
assert(引擎, "引擎未启动。")
local _线程列表 = 引擎.线程列表
local __ggeThread = require("gge_thread")

local GGELUA = class()

function GGELUA:初始化(脚本)
    _线程列表[self] = self
    self.obj = __ggeThread(脚本,self)
end

function GGELUA:启动(队列数)
    return self.obj:Start(队列数 or 1000)
end

function GGELUA:停止()
    self.obj:Stop()
end
-- function GGELUA:同步发送(...)
--     if self.是否启动 then
--         self.obj:SyncSend(...)
--     end
-- end
function GGELUA:发送(data)--只能发送string
    return self.obj:Send(data)
end
function GGELUA:置延迟(v)
    self.obj:SetSleep(v)
end
function GGELUA:OnMessage(...)
    if self.消息函数 then
        return __gge.xpcall(self.消息函数,self,...)
    end
end


return GGELUA