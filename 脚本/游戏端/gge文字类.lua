-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-30 17:39:31

--<<=========================================================================================>>--
--local _tostring = function (t) return "ggeFont",tostring(t._obj) end
--<<=========================================================================================>>--
local _文字列表 = 引擎 and 引擎.文字列表
local GGELUA    = class(require"gge引用")
GGELUA._new     = require("gge_font")

function GGELUA:初始化(file,size,bold,border,mono)
    local mode = 4--默认关抗锯齿
    if bold    then    mode = bit.bor(mode,1) end--粗体
    if border  then    mode = bit.bor(mode,2) end--描边
    if mono    then    mode = bit.bxor(mode,4)end--抗锯齿

    if(type(file) == "userdata")then
        --self._obj = file
    elseif type(file) == 'string' then
        if size then
            self._obj = self._new(file,size,mode)
        else--从图片创建字体
            self._obj = self._new(file,0,mode)--Font_CreateFromImage
        end
    else
        local file = file or os.getenv("SystemRoot")..'/Fonts/simsun.ttc'
        local size = size or 14
        self._obj = self._new(file,size,mode)
    end

    --getmetatable(self).__tostring = _tostring
    if _文字列表 then _文字列表[self] = self end
end
function GGELUA:克隆()
    return GGELUA(self._obj:Clone())
end
function GGELUA:显示(x,y,t)
    if type(x)=='table' then
        t   = y
        x,y = x.x or 0,x.y or 0
    end
    self._obj:Render(x,y,tostring(t) or "nil")
end
function GGELUA:输出(x,y,f,...)
    if type(x)=='table' then
        t   = y
        x,y = x.x or 0,x.y or 0
    end
    self._obj:Render(x,y,string.format(f, ...))
end
function GGELUA:置颜色(v)
    self._obj:SetColor(v)
    return self
end
function GGELUA:置Z轴(v)
    self._obj:SetZ(v)
    return self
end
function GGELUA:置混合(v)
    self._obj:SetBlendMode(v)
    return self
end
function GGELUA:置行宽(v)
    self._obj:SetLineWidth(v)
    return self
end
function GGELUA:置字间距(v)
    self._obj:SetCharSpace(v)
    return self
end
function GGELUA:置行间距(v)
    self._obj:SetLineSpace(v)
    return self
end
function GGELUA:置限制字数(v)
    self._obj:SetCharNum(v or -1)
    return self
end
    -- /// 字体排版样式
local FONT_ALIGN =
{
 左对齐   =0;
 居中对齐 =1;
 右对齐   =2
};
function GGELUA:置样式(v)
    self._obj:SetAlign(FONT_ALIGN[v])
    return self
end
function GGELUA:置阴影颜色(v)--颜色
    self._obj:SetShadowColor(v)
    return self
end
function GGELUA:置描边颜色(v)--颜色
    self._obj:SetBorderColor(v)
    return self
end
--=========================================================================
function GGELUA:取颜色()
    return self._obj:GetColor()
end
function GGELUA:取Z轴()
    return self._obj:GetZ()
end
function GGELUA:取混合()
    return self._obj:GetBlendMode()
end
function GGELUA:取行宽()
    return self._obj:GetLineWidth()
end
function GGELUA:取字间距()
    return self._obj:GetCharSpace()
end
function GGELUA:取行间距()
    return self._obj:GetLineSpace()
end
function GGELUA:取大小()--取字体大小
    return self._obj:GetFontSize()
end
function GGELUA:取限制字数()
    return self._obj:GetCharNum()
end
function GGELUA:取样式()
    return self._obj:GetAlign()
end
function GGELUA:取阴影颜色()--取阴影颜色
    return self._obj:GetShadowColor()
end
function GGELUA:取描边颜色()--取描边颜色
    return self._obj:GetBorderColor()
end
function GGELUA:取宽高(str,len)
    return self._obj:GetStringInfo(str,len or -1)
end
function GGELUA:取宽度(str,len)
    local w = self._obj:GetStringInfo(str,len or -1)
    return w
end
function GGELUA:取高度(str,len)
    local _,h = self._obj:GetStringInfo(str,len or -1)
    return h
end
return GGELUA