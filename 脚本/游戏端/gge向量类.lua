-- @Author: baidwwy
-- @Date:   2018-04-16 22:26:46
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-17 03:46:23
local __Vector = require("gge_Vector3")
local GGELUA = class()


function GGELUA:初始化(x,y,z)
    if type(x) == 'number' then
        self._obj = __Vector(x,y,z)
    elseif type(x) == 'userdata' then
        self._obj = x
    end
end

-- @brief 获得两矢量点积
-- @param v 矢量
-- @return 两矢量点积
function GGELUA:Dot(v)
    return self._obj:Dot(v:取对象())
end
-- @brief 获得两矢量叉积
-- @param v 矢量
-- @return 两矢量叉积
function GGELUA:Cross(v)
    return GGELUA (self._obj:Cross(v:取对象()))
end
--@brief 标准化矢量
function GGELUA:Normalize()
    self._obj:Normalize()
    return self
end
-- @brief 获得矢量长度
-- @return 矢量长度
function GGELUA:Length()
    return self._obj:Length()
end
-- @brief 获得矢量长度的平方
-- @return 矢量长度的平方
function GGELUA:LengthSquared()
    return self._obj:LengthSquared()
end
-- @brief 获得两矢量间角度
-- @param v 矢量
-- @return 两矢量间角度
function GGELUA:Angle(v)
    return self._obj:Angle(v:取对象())
end
-- @brief 使矢量长度不超过指定的长度
-- @param max 最大长度
function GGELUA:Clamp(v)
    return self._obj:Clamp(v)
end

function GGELUA:取对象()
    return self._obj
end
return GGELUA