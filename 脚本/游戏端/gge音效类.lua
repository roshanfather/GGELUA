-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-16 23:16:40
local __向量类 = require("gge向量类")
--<<=========================================================================================>>--
local __ggeSound = require "gge_sound"
--local _tostring = function (t) return "ggeSound",tostring(t._obj)
--<<=========================================================================================>>--
local _音效列表 = 引擎 and 引擎.音效列表
local GGELUA = class(require'gge引用')

function GGELUA:初始化(file,size)
    self._obj = __ggeSound()
    if type(file) == "userdata" then
        self._obj:Sound_Pointer(file)
    elseif size then
        self._obj:Sound_LoadMem(file,size)
    else
        self._obj:Sound_LoadFile(file)
    end

    --getmetatable(self).__tostring = _tostring
    if _音效列表 then _音效列表[self] = self end
end
--是否重复
function GGELUA:播放(loop)
    self._obj:Play()
    if loop then
        self._obj:SetLoop(true)
    end
    return self
end
--顾名思义
function GGELUA:暂停()
    self._obj:Pause()
end
--顾名思义
function GGELUA:恢复()
    self._obj:Resume()
end
--顾名思义
function GGELUA:停止()
    self._obj:Stop()
end
--@brief 返回是否正在播放
function GGELUA:是否播放()
    return self._obj:IsPlaying()
end
--@brief 返回时长(单位:秒)
function GGELUA:取时长()
    return self._obj:GetDuration()
end
function GGELUA:置重复()
    self._obj:SetLoop()
end
function GGELUA:是否重复()
    return self._obj:IsLoop()
end
-- @brief 设置音量
-- @param volume 设置音量，范围0～100
function GGELUA:置音量(volume)
    self._obj:SetVolume(volume or 100)
end
function GGELUA:取音量()
    return self._obj:GetVolume()
end
function GGELUA:置频率(pitch)
    self._obj:SetPitch(pitch)
end
function GGELUA:取频率()
    return self._obj:GetPitch()
end
-- @brief 设置音源坐标是否是相对坐标
-- @param bRelative 设为true时音源坐标为相对于监听器的坐标
function GGELUA:置相对坐标(bRelative)
    self._obj:SetRelativeToListener(bRelative)
end
function GGELUA:是否相对坐标()
    return self._obj:IsRelativeToListener()
end
-- @brief 设置音源坐标
-- @param position 音源坐标
function GGELUA:置坐标(position)
    self._obj:SetPosition(position:取对象())
end
function GGELUA:取坐标()
    return __向量类(self._obj:GetPosition())
end
-- @brief 设置音源速度
-- @param velocity 音源速度
function GGELUA:置速度(velocity)
    self._obj:SetVelocity(velocity:取对象())
end
function GGELUA:取速度()
    return __向量类(self._obj:GetVelocity())
end
-- @brief 设置音源方向
-- @param direction 音源方向
function GGELUA:置方向(direction)
    self._obj:SetDirection(direction:取对象())
end
function GGELUA:取方向()
    return __向量类(self._obj:GetDirection())
end
-- @brief 设置音源锥形
-- @param innerAngle 内锥角度，默认360
-- @param outerAngle 外锥角度，默认360
-- @param outerVolume 外锥音量，范围0～1，1表示音量
function GGELUA:置锥形(innerAngle,outerAngle,outerVolume)
    self._obj:SetCone(innerAngle,outerAngle,outerVolume)
end
function GGELUA:取锥形()
    return self._obj:GetCone()
end
-- @brief 设置音源最小距离
-- @param distance 音源最小距离
function GGELUA:置最小距离(distance)
    self._obj:SetMinDistance(distance)
end
function GGELUA:取最小距离()
    return self._obj:GetMinDistance()
end
-- @brief 设置音源最大距离
-- @param distance 音源最大距离
function GGELUA:置最大距离(distance)
    self._obj:SetMaxDistance(distance)
end
function GGELUA:取最大距离()
    return self._obj:GetMaxDistance()
end
-- @brief 设置音源音量衰减速度
-- @param factor 音源衰减速度，越大衰减越快，默认1
function GGELUA:置衰减速度(factor)
    self._obj:SetRolloffFactor(factor)
end
function GGELUA:取衰减速度()
    return self._obj:GetRolloffFactor()
end
return GGELUA