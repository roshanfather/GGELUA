-- @作者: baidwwy
-- @邮箱:  313738139@qq.com
-- @创建时间:   2016-04-23 08:41:18
-- @最后修改来自: baidwwy
-- @Last Modified time: 2017-07-29 00:26:40
local GUI基类 = class()

function GUI基类:初始化(名称,x,y,宽度,高度,父)
    self._id       = -1
    self._名称     = 名称
    self._x        = x or 0
    self._y        = y or 0
    self._px       = x or 0--偏移
    self._py       = y or 0--当控件移动，偏移就有用
    self._fx       = 0
    self._fy       = 0
    self._宽度     = 宽度 or 0
    self._高度     = 高度 or 0
    self._类型     = '控件'
    self._焦点     = false
    self._鼠标按下 = false
    self._可视     = true
    self._禁止     = false
    self._层次     = 0
    self._加载     = false --是否已经加载
    self._调试     = false
    self._包围盒   = require("gge包围盒")(0,0,self._宽度,self._高度)
    self._包围盒:置中心(-self._px,-self._py)

    if 父 then
        self._根控件 = 父._根控件
        self._父窗口 = 父._父窗口
        self._父控件 = 父

        self._fx     = 父._x
        self._fy     = 父._y--父坐标
        self._x      = self._px+self._fx
        self._y      = self._py+self._fy--偏移+父 = 绝对坐标
        self._包围盒:置坐标(self._fx,self._fy)
    end
end
function GUI基类:取文字()
    return self._文字
end
function GUI基类:置调试模式(v)
    self._调试 =v
    return self
end
function GUI基类:置坐标(x,y)
    self._px = x or self._px
    self._py = y or self._py
    self._x  = self._px+self._fx
    self._y  = self._py+self._fy
    if not self._精灵 or self._包围盒 ~= self._精灵:取包围盒() then--置纹理后，替换了包围盒
        self._包围盒:置中心(-self._px,-self._py)
    end
    if self._类型 == '控件' or self._类型 == '窗口' then
        self:_消息事件('坐标事件',{self._x,self._y})--告诉子控件
    end
    return self
end
function GUI基类:取坐标()
    return self._x,self._y
end
function GUI基类:取坐标表()
    return require("gge坐标类")(self._x,self._y)
end
function GUI基类:置宽高(w,h)
    self._宽度 = w
    self._高度 = h
    self._包围盒:置宽高(w,h)
    return self
end
function GUI基类:取名称()
    return self._名称
end
function GUI基类:取类型()
    return self._类型
end
function GUI基类:置类型(v)
    self._类型 = v
    return self
end
function GUI基类:取父控件()
    return self._父控件
end
function GUI基类:取根控件()--层
    return self._根控件
end
function GUI基类:取所在窗口()--控件所属窗口
    return self._父窗口
end
function GUI基类:置包围盒(v)
    self._包围盒   = v
    return self
end
function GUI基类:取包围盒()
    return self._包围盒
end
function GUI基类:检查点(x,y)
    return self._包围盒:检查点(x,y)
end
function GUI基类:取层次()
    return self._层次
end
function GUI基类:置层次(v)
    self._层次    = v
    return self
end
function GUI基类:加载()
    self._加载    = true
    if self.初始化 then self:初始化()end
end
function GUI基类:置可视(v,sub)
    if not self._加载 and v then
        self:加载(sub)
    end
    -- if not self:_消息事件('可视请求',{v,self}) then
    --     print(self._名称)
    -- end
    if self._可视 ~= v then
        self._可视 = v
        self:_消息事件('可视事件',{self._可视,self})
    end
    return self
end
function GUI基类:置禁止(v)
    -- self._焦点         = false
    -- self._鼠标按下   = false
    self._禁止        = v
    self:_消息事件('禁止事件',{v})
    return self
end
function GUI基类:是否禁止()
    return self._禁止
end
function GUI基类:是否可视()
    return self._可视
end
function GUI基类:是否加载()
    return self._加载
end
function GUI基类:是否焦点()
    return self._焦点
end
function GUI基类:发送消息(...)
    if self.加载 then
        if self.消息事件 then
            return self:消息事件(...)
        end
    end
    return false
end
function GUI基类:_消息事件(...)
end
function GUI基类:_子消息事件(...)
end
return GUI基类