--=========================================================================
local GUI网格类 = class(require "gge界面/基类")
--=========================================================================
function GUI网格类:初始化(标记,x,y,宽度,高度)
    self._类型   = '网格'
    self._行距   = 0
    self._列距   = 0
    self._格子宽 = 32
    self._格子高 = 32
    self._行数   = 0
    self._列数   = 0
    self._内容   = {}
    self._精灵   = require("gge精灵类")()
    self._格子   = {}
end
function GUI网格类:置格子宽高(a,b)
    self._格子宽 = a
    self._格子高 = b
    return self
end
function GUI网格类:置格子中心(i,x,y)
    if self._格子[i] then
        self._格子[i]:置中心(-(x+self._px), -(y+self._py))
    end
    return self
end
function GUI网格类:置坐标(x,y)
    self[require("gge界面/基类")]:置坐标(x,y)
    local n = 1
    for h=1,self._行数 do
        for l=1,self._列数 do
            self._格子[n]:置中心(-((l-1)*(self._格子宽+self._列距)+self._px), -((h-1)*(self._格子高+self._行距)+self._py))
            n=n+1
        end
    end
    return self
end
function GUI网格类:置行列间距(a,b)
    self._行距   = a
    self._列距   = b
    local n = 1
    for h=1,self._行数 do
        for l=1,self._列数 do
            self._格子[n]:置中心(-((l-1)*(self._格子宽+self._列距)+self._px), -((h-1)*(self._格子高+self._行距)+self._py))
            n=n+1
        end
    end
    return self
end
function GUI网格类:添加格子(x,y,w,h)
    local b = require("gge包围盒")(0,0,w,h)
    b:置中心(-(x+self._px), -(y+self._py))
    b:置坐标(self._fx,self._fy)
    table.insert(self._格子, b)
end
function GUI网格类:删除格子(i)
    table.remove(self._格子, i)
    return self
end
function GUI网格类:置行列数(a,b)
    self._行数   = a
    self._列数   = b
    for h=1,self._行数 do
        for l=1,self._列数 do
            self:添加格子(
                (l-1)*(self._格子宽+self._列距),
                (h-1)*(self._格子高+self._行距),
                self._格子宽,self._格子高)
        end
    end
    return self
end
function GUI网格类:置内容(i,t)
    self._内容[i] = t
    return self
end
function GUI网格类:取内容(i)
    return self._内容[i]
end
function GUI网格类:取格子(i)
    return self._格子[i]
end
function GUI网格类:取格子坐标(id)
    return self._格子[id]:取坐标()
end
function GUI网格类:取格子数量()
    return #self._格子
end
function GUI网格类:清空格子()
    self._格子   = {}
    return self
end
function GUI网格类:清空内容()
    self._内容    = {}
    return self
end
function GUI网格类:清空()
    self._内容    = {}
    return self
end
function GUI网格类:遍历格子()
    return next,self._格子
end
function GUI网格类:遍历内容()
    return next,self._内容
end
function GUI网格类:_显示(x,y)
    if self.底显示 then self:底显示(self._x,self._y,x,y)end
    for i,v in ipairs(self._格子) do
        if self._调试 then v:显示() end
        if self._内容[i] then
            if tostring(self._内容[i])=='ggeTexture' then
                self._精灵:置纹理(self._内容[i])
                self._精灵:显示(v:取坐标())
            elseif self._内容[i].显示 then
                self._内容[i]:显示(v:取坐标())
            end
        end
        if self.子显示 then self:子显示(i,v:取坐标()) end
    end
    if self.显示   then self:显示(self._x,self._y,x,y)  end
    if self._调试 then self._包围盒:显示(0xFFFFFF00) end
end
function GUI网格类:检查格子(x,y)
    for i,v in ipairs(self._格子) do
        if v:检查点(x,y) then
            return i
        end
    end
    return false
end
function GUI网格类:_消息事件(类型,参数)
    local x,y = unpack(参数)
    if 类型 == '坐标事件' then
        self._x,self._y = self._px+x,self._py+y
        self._包围盒:置坐标(x,y)
        for i,v in ipairs(self._格子) do
            v:置坐标(x,y)
        end
    elseif 类型 == '鼠标移动' then
        if not 参数.碰撞 and self:检查点(x,y) then
            self:发送消息(类型,x,y)
            if not 参数.左键按住 then
                local i = self:检查格子(x,y)
                if i then
                    if self._当前格子 ~= i then
                        if self._当前格子 then self:发送消息('失去格子焦点',self._当前格子) end
                        self._当前格子 = i
                        self:发送消息('获得格子焦点',i)
                    end
                    return true
                end
            end
            self._焦点 = true
            self:发送消息('获得焦点',x,y)
        elseif self._焦点 then
            self:发送消息('失去焦点',x,y)
        end
        if self._当前格子 then
            self:发送消息('失去格子焦点',self._当前格子);
            self._当前格子=nil
        end
    elseif 类型 == '左键按下' then
        if  self:检查点(x,y) then
            if not self:是否禁止() then
                self._鼠标左键按下 = true
                self:发送消息(类型,x,y)
                local i = self:检查格子(x,y)
                if i then
                    self:发送消息("格子左键按下",i)
                    return true
                end
            end
        end
    elseif 类型 == '左键弹起' then
        if self:检查点(x,y) then
            self:发送消息(类型,x,y)
            if self._鼠标左键按下 then
                self._鼠标左键按下 = false
                local i = self:检查格子(x,y)
                if i then
                    self:发送消息("格子左键弹起",i)
                    return true
                end
            end
        end
    elseif 类型 == '右键按下' then
        if  self:检查点(x,y) then
            if not self:是否禁止() then
                self._鼠标右键按下 = true
                self:发送消息(类型,x,y)
                local i = self:检查格子(x,y)
                if i then
                    self:发送消息("格子右键按下",i)
                    return true
                end
            end
        end
    elseif 类型 == '右键弹起' then
        if  self._鼠标右键按下 and self:检查点(x,y) then
            self._鼠标右键按下 = false
            if not self:是否禁止() then
                self:发送消息(类型,x,y)
                local i = self:检查格子(x,y)
                if i then
                    self:发送消息("格子右键弹起",i)
                    return true
                end
            end
        end
    else
        return self:发送消息(类型,unpack(参数))
    end
end
return GUI网格类