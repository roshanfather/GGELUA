-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-30 17:57:13
--gge精灵类
--<<=========================================================================================>>--
local _tostring = function (t) return "sprite",tostring(t._obj) end
--<<=========================================================================================>>--
local _精灵列表 = 引擎 and 引擎.精灵列表
local _纹理列表 = setmetatable({}, {__mode='k'})
local GGELUA = class(require"gge引用")
GGELUA._new_ = require("gge_sprite")

function GGELUA:初始化(tex,x,y,w,h)
    self._obj = self._new_()

    if type(tex) == "table" and tostring(tex)=='texture' then
        self._obj:Sprite_Create(tex:取指针(),x or 0 ,y or 0,w or 0,h or 0)
        _纹理列表[self] = tex
    elseif type(tex) == "string" then
        tex = require("gge纹理类")(tex)
        self._obj:Sprite_Create(tex:取指针(),x or 0 ,y or 0,w or 0,h or 0)
        _纹理列表[self] = tex
    elseif type(tex) == "userdata" then
        self._obj:Sprite_Pointer(tex)
    else
        self._obj:Sprite_Create(nil,x or 0 ,y or 0,w or 0,h or 0)
    end
    self._rect = require("gge包围盒")()
    getmetatable(self).__tostring = _tostring
    if _精灵列表 then _精灵列表[self] = self end
end
-- @brief 返回当前精灵的一个副本
-- @return 当前精灵的一个副本
function GGELUA:克隆()
    return GGELUA(self._obj:Clone())
end
-- @brief 渲染
-- @param x x坐标
-- @param y y坐标
function GGELUA:显示(x,y)
    if type(x) == 'table' then
        x,y=x.x or 0,x.y or 0
    end
    self._obj:SetPosition(x or 0,y or 0)
    self._obj:RenderPosition()
    if self._高亮 then
        self._obj:SetBlendMode(2)
        self._obj:SetColor(self._高亮颜色,-1)
        self._obj:RenderPosition()
        self._obj:SetBlendMode(0)
        self._obj:SetColor(self._颜色,-1)
    end
    return self
end
--旋转弧度数
function GGELUA:置旋转(rotation)--默认0
    self._obj:SetRotation(rotation)
    return self
end
--水平缩放系数,垂直缩放系数
function GGELUA:置缩放(hscale,vscale)--默认1
    self._obj:SetScale(hscale,vscale or 0)
    return self
end
function GGELUA:置拉伸(width,height)--默认0
    self._obj:SetStretch(width,height)
    return self
end
-- @brief 设置渲染坐标
-- @param x0 左上角x坐标
-- @param y0 左上角y坐标
-- @param x1 右上角x坐标
-- @param y1 右上角y坐标
-- @param x2 左下角x坐标
-- @param y2 左下角y坐标
-- @param x3 右下角x坐标
-- @param y3 右下角y坐标
function GGELUA:置顶点(...)
    local arg = {...}

    local x,y,x1,y1,x2,y2,x3,y3
    if type(arg[1]) == 'table' then
        x,y,x1,y1,x2,y2,x3,y3=arg[1].x,arg[1].y,arg[2].x1,arg[2].y,arg[3].x,arg[3].y,arg[4].x,arg[4].y
    else
        x,y,x1,y1,x2,y2,x3,y3 = unpack(arg)
    end
    self._obj:SetPosition4V(x,y,x1,y1,x2,y2,x3,y3)

    return self
end
function GGELUA:置纹理(tex)
    if tex and tostring(tex)=='texture' then
        _纹理列表[self] = tex
        self._obj:SetTexture(tex:取指针())
    else
        self._obj:SetTexture(0)
    end
    return self
end
function GGELUA:取纹理()--GetTexture
    return _纹理列表[self]
end
-- @brief 设置渲染纹理区域
-- @param x x坐标
-- @param y y坐标
-- @param width 宽度
-- @param height 高度
function GGELUA:置区域(x,y,width,height)
    self._obj:SetTextureRect(x,y,width,height)
    return self
end
function GGELUA:取区域()
    return self._obj:GetTextureRect()
end

-- @brief 设置渲染颜色
-- @param color 渲染颜色
-- @param i 要设置的顶点(0~3)，-1则四个顶点都设置为该值
function GGELUA:置颜色(color,i)
    self._obj:SetColor(color,i or -1)
    return self
end
function GGELUA:取颜色(i)
    return self._obj:GetColor(i or 0)
end
-- @brief 设置Z轴
-- @param z 0.0f～1.0f，0表示最上层，1表示最下层
-- @param i 要设置的顶点(0~3)，-1则四个顶点都设置为该值
function GGELUA:置Z轴(v,i)
    self._obj:SetZ(v,i or -1)
    return self
end
function GGELUA:取Z轴(i)
    return self._obj:GetZ(i or 0)
end
--@brief 设置混合模式
function GGELUA:置混合(blend)
     self._obj:SetBlendMode(blend or 0)
    return self
end
function GGELUA:取混合()
     return self._obj:GetBlendMode()
end
-- @brief 设置锚点
-- @param x x坐标
-- @param y y坐标
function GGELUA:置中心(x,y)
    self._obj:SetHotSpot(x,y);
    return self
end
function GGELUA:取中心()--GetHotSpot
    return self._obj:GetHotSpot()
end
-- @brief 设置纹理翻转属性
-- @param bX 水平翻转
-- @param bY 垂直翻转
-- @param bHotSpot 是否翻转原点
function GGELUA:置翻转(x,y,bHotSpot)
    self._obj:SetFlip(x,y,bHotSpot)
end
function GGELUA:取翻转()--GetFlip
    return self._obj:GetFlip()
end
--@brief 获得宽度
function GGELUA:取宽度()
    return self._obj:GetWidth()
end
--@brief 获得高度
function GGELUA:取高度()
    return self._obj:GetHeight()
end
-- @brief 获得精灵的包围盒
-- @param rect 保存包围盒矩形至该参数
-- @param x 包围盒左上角x坐标
-- @param y 包围盒左上角y坐标
-- @return 包围盒矩形
function GGELUA:取包围盒()
    self._obj:GetBoundingBox(self._rect:取指针())
    return self._rect
end
-- @brief 获得旋转和翻转后的精灵包围盒
-- @param rect 保存包围盒矩形至该参数
-- @param x 保存包围盒矩形至该参数
-- @param y 保存包围盒矩形至该参数
-- @param rotation 旋转弧度数,0.0f设为默认
-- @param hscale 水平缩放系数,1.0f设为默认
-- @param vscale 垂直缩放系数,1.0f设为默认
-- @return 包围盒矩形
-- function GGELUA:取包围盒2(x,y)
--     self._obj:GetBoundingBoxEx(x,y)
-- end

--======================================================================================
--扩展
--======================================================================================
function GGELUA:取像素(x,y)
    if type(x) == 'table' then x,y=x.x,x.y end
    if _纹理列表[self] and x and y and self:检查点(x,y) then
        local mx,my = self._obj:GetPosition()
        local hx,hy = self:取中心()
        return _纹理列表[self]:取像素(x-mx+hx,y-my+hy)
    end
    return 0
end
function GGELUA:检查点(x,y)
    if type(x) == 'table' then x,y=x.x,x.y end
    return self:取包围盒():检查点(x,y)
end
function GGELUA:检查像素(x,y)
    return self:取像素(x,y)~=0
end
function GGELUA:灰度级()
    if _纹理列表[self] then
        _纹理列表[self]:灰度级()
    end
    return self
end
function GGELUA:置高亮(v,c)
    self._高亮 = v
    self._高亮颜色 = c or self._高亮颜色
    return self
end
function GGELUA:取高亮()
    return self._高亮
end

function GGELUA:置过滤(v)
    self._obj:SetTextureFilter(v)
    return self
end
function GGELUA:是否过滤()
    return self._obj:IsTextureFilter()
end
--释放后需要collectgarbage()才会即时生效
function GGELUA:释放(tex)
    if  tex and _纹理列表[self] then
        _纹理列表[self]:释放()
    end
    self._isok = self._obj:Release()
end

return GGELUA