-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-12 17:23:00

--这是一个基类
local GGELUA = class()

function GGELUA:取对象()
    return self._obj
end
function GGELUA:取指针()
	return self._obj:GetP()
end
function GGELUA:置指针(p)
	self._obj:SetP(p)
end
function GGELUA:取引用总数()
	return self._obj:GetRefCount()
end
function GGELUA:加引用()
	self._obj:AddRef()
    return self
end
--释放后需要collectgarbage()才会即时生效
function GGELUA:释放()
	return self._obj:Release()
end
function GGELUA:是否有效()
    return tostring(self._obj:GetP())~='userdata: NULL'
end
return GGELUA