-- @Author: baidwwy
-- @Date:   2018-04-10 22:51:52
-- @Last Modified by:   baidwwy
-- @Last Modified time: 2018-04-16 22:23:52

local __Vector = require("gge_Vector")
local GGELUA = class()
--只有对比函数相同才可以触发比较
local _eq       = function (a,b) return a.x==b.x and a.y==b.y end
local _sub      = function (a,b) return GGELUA(a.x - b.x,a.y - b.y) end
local _mul      = function (a,b) return GGELUA(a.x * b.x,a.y * b.y) end
local _div      = function (a,b) return GGELUA(a.x / b.x,a.y / b.y) end
local _add      = function (a,b) return GGELUA(a.x + b.x,a.y + b.y) end
--local _tostring = function (a,b) return "ggeGGELUA",string.format("%s,%s",a.x,a.y) end
--local _concat   = function (a,b) return string.format("%s,%s",a.x + b.x,a.y + b.y) end
--__mod %
--__unm
--__lt <
--__le <=
function GGELUA:初始化(x,y)
    self.x = tonumber(x) or 0
    self.y = tonumber(y) or 0

    local mt = getmetatable(self)
        mt.__eq         = _eq
        mt.__add        = _add
        mt.__sub        = _sub
        mt.__mul        = _mul
        mt.__div        = _div
        --mt.__tostring     = _tostring
        --mt.__concat     = _concat
end
function GGELUA:ceil()
    self.x = math.ceil(self.x)
    self.y = math.ceil(self.y)
    return self
end
function GGELUA:floor()
    self.x = math.floor(self.x)
    self.y = math.floor(self.y)
    return self
end
function GGELUA:unpack()
    return self.x,self.y
end
function GGELUA:更新(x,y)
    self.x = y and x or x.x
    self.y = y and y or x.y
    return self
end
function GGELUA:复制()
    return GGELUA(self.x,self.y)
end
function GGELUA:随机(x,x1,y,y1)
    self.x = math.random(x, x1)
    self.y = math.random(y, y1)
    return self
end
--取两点距离
function GGELUA:取距离(p)
    return math.sqrt (math.pow(self.x-p.x,2) + math.pow(self.y-p.y,2))
end
--取两点弧度
local pi2 =  math.pi*2
function GGELUA:取弧度(p)
    local x = self.x
    local y = self.y
    if(p.y ==y and p.x==x)then
        return 0
    elseif(p.y >=y and p.x<=x)then
        return math.pi-math.abs(math.atan((p.y-y)/(p.x-x)))
    elseif(p.y <=y and p.x>=x)then
        return pi2-math.abs(math.atan((p.y-y)/(p.x-x)))
    elseif(p.y <=y and p.x<=x)then
        return math.atan((p.y-y)/(p.x-x))+math.pi
    elseif(p.y >=y and p.x>=x)then
        return math.atan((p.y-y)/(p.x-x))
    end
end
function GGELUA:取角度(p)--math.rad
    return math.deg(self:取弧度(p))
end
function GGELUA:取地图位置(w,h)--图块宽高
    return GGELUA(math.ceil(self.x/w), math.ceil(self.y/h))
end
function GGELUA:取距离坐标(r,a) --距离,孤度
    local x,y = 0,0
    x=r* math.cos(a)+self.x
    y=r* math.sin(a)+self.y
    return GGELUA(x,y)
end
function GGELUA:取移动坐标(dst,r)--目标点,距离
    local a = self:取弧度 (dst)
    return self:取距离坐标(r,a)
end
function GGELUA:取屏幕坐标(w,h) --地图宽高
    local w2,h2 = 引擎.宽度2,引擎.高度2--窗口宽高的一半
    local rx,ry = 0,0
    local x,y = self.x,self.y
    if w >引擎.宽度 then
        if (x>w2 and x<w-w2) then
            rx = -(x-w2)
        elseif x<=w2 then
            rx=0
        elseif x>=w-w2 then
            rx=-(w-引擎.宽度)
        end
    end
    if h > 引擎.高度 then
        if (y>h2 and y<h-h2) then
            ry = -(y-h2)
        elseif y<=h2 then
            ry=0
        elseif y>=h-h2 then
            ry=-(h-引擎.高度)
        end
    end
    return GGELUA(rx,ry)
end

function GGELUA:画线(p,color)
    local draw = 引擎.画线
    color = color or 0xFFFF0000
    draw(self.x,self.y,p.x,p.y,color)
    return self
end
function GGELUA:画圆(...)
    self:显示(...)
end
function GGELUA:画矩形(p,color)

end
--@参数 半径,边数,颜色
function GGELUA:显示(r,num,color)
    r = r or 3
    num = num or 20
    color = color or 0xFFFF0000
    local a = 360/num
    local pxy,pxy1
    local draw = 引擎.画线
    for i=0,num-1 do
        pxy = self:取距离坐标(r,math.rad(i*a))--rad角度转弧度
        pxy1 = self:取距离坐标(r,math.rad((i+1)*a))
        draw(pxy.x,pxy.y,pxy1.x,pxy1.y,color)
    end
    return self
end

return GGELUA